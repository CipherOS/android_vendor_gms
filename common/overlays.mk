DEVICE_PACKAGE_OVERLAYS += \
    vendor/gms/overlay

PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += \
    vendor/gms/overlay

PRODUCT_PACKAGES += \
    PixelBuiltInPrintService \
    CaptivePortalLoginOverlay \
    CellBroadcastReceiverOverlay \
    CellBroadcastServiceOverlay \
    PixelContactsProvider \
    GoogleConfigOverlay \
    GooglePermissionControllerOverlay \
    GoogleWebViewOverlay \
    ManagedProvisioningPixelOverlay \
    NetworkStackOverlay \
    PixelConfigOverlay2018 \
    PixelConfigOverlay2019 \
    PixelConfigOverlay2019Midyear \
    PixelConfigOverlayCommon \
    PixelConnectivityOverlay2021 \
    PixelSetupWizardOverlay \
    PixelSetupWizard_rro \
    PixelTetheringOverlay2021 \
    PixelSettingsGoogle \
    PixelSettingsProvider \
    SystemUIGXOverlay \
    PixelSystemUIGoogle \
    PixelTeleService \
    PixelTelecom \
    Pixelframework-res \
